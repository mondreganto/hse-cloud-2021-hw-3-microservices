class HealthCheckMixin:
    @staticmethod
    def health():
        return {'status': 'ok'}
