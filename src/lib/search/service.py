from abc import abstractmethod
from typing import Optional

import pandas as pd


def stupid_count_tokens(tokens, text):
    res = 0
    for token in tokens:
        if token in text:
            res += 1
    return res


class BaseSearchService:
    DOCS_COLUMNS = ['document', 'key', 'key_md5']

    @abstractmethod
    def get_search_data(self, search_text: str, user_data: Optional[dict] = None, limit: int = 10) -> pd.DataFrame:
        pass


class CommonSearchMixin:
    _data: pd.DataFrame

    def _build_tokens_count(self, search_text: str):
        tokens = search_text.split()
        res = self._data['document'].apply(lambda x: stupid_count_tokens(tokens, x))
        res.name = None
        return res

    def _get_gender_mask(self, user_data: Optional[dict] = None):
        ud = user_data.get('gender', 'null') if user_data is not None else 'non-existing gender'
        return self._data['gender'].apply(lambda x: stupid_count_tokens([ud], x))

    def _get_age_mask(self, user_data: Optional[dict] = None):
        user_age = int(user_data['age']) if user_data is not None else -1
        return self._data.apply(lambda x: x['age_from'] <= user_age <= x['age_to'], axis=1)

    @staticmethod
    def _sort_by_rating_and_tokens(rating, tokens_count, key_md5):
        df = pd.concat([tokens_count, rating, key_md5], axis=1)
        return df.sort_values([0, 1, 'key_md5'], ascending=[False, False, False])
