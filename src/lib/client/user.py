from urllib.parse import urljoin

import requests


class UserClient:
    def __init__(self, url: str):
        self._url = url

    def get_url(self, path: str):
        return urljoin(self._url, path)

    def get_data(self, user_id: int):
        url = self.get_url(f'/users')
        r = requests.get(url, params={'user_id': user_id})
        r.raise_for_status()
        return r.json()
