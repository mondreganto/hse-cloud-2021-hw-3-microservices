from urllib.parse import urljoin

import pandas as pd
import requests
from pydantic import BaseModel
from typing import Optional


class SearchRequest(BaseModel):
    search_text: str
    user_data: Optional[dict]
    limit: int = 10


class SearchClient:
    DOCS_COLUMNS = ['document', 'key', 'key_md5']

    def __init__(self, url: str):
        self._url = url

    def get_url(self, path: str) -> str:
        return urljoin(self._url, path)

    def get_data(self, request: SearchRequest) -> pd.DataFrame:
        url = self.get_url('/search')
        r = requests.post(url, json=request.dict())
        r.raise_for_status()
        return pd.read_json(r.text)
