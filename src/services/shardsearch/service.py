from abc import abstractmethod
from typing import List

import pandas as pd
from typing import Optional

from lib.client.search import SearchClient, SearchRequest
from lib.search.service import BaseSearchService, CommonSearchMixin


def stupid_count_tokens(tokens, text):
    res = 0
    for token in tokens:
        if token in text:
            res += 1
    return res


class SearchInShardsService(BaseSearchService, CommonSearchMixin):
    def __init__(self, shards: List[SearchClient]):
        self._shards = shards

    def get_search_data(self, search_text: str, user_data: Optional[dict] = None, limit: int = 10) -> pd.DataFrame:
        if search_text is None or search_text == '':
            return pd.DataFrame([], columns=self.DOCS_COLUMNS)

        shards_responses = []
        request = SearchRequest(
            search_text=search_text,
            user_data=user_data,
            limit=limit,
        )
        for shard in self._shards:
            shards_responses.append(shard.get_data(request))

        self._data = pd.concat(shards_responses)  # possible data race in case of multi thread/async usage
        self._data.reset_index(inplace=True, drop=True)
        assert self._data.index.is_unique

        tokens_count = self._build_tokens_count(search_text)
        gender_mask = self._get_gender_mask(user_data)
        age_mask = self._get_age_mask(user_data)
        rating = gender_mask + age_mask
        df = self._sort_by_rating_and_tokens(rating, tokens_count, self._data['key_md5'])
        return self._data.loc[df.head(limit).index]
