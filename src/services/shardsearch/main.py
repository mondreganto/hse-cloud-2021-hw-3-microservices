from server import Server
from lib.client.search import SearchClient
from service import SearchInShardsService
from settings import SHARDS_ADDRESSES


def main():
    search_service = SearchInShardsService(shards=list(map(SearchClient, SHARDS_ADDRESSES)))
    server = Server('shardsearch', search_service=search_service)
    server.run_server(debug=True)


if __name__ == '__main__':
    main()
