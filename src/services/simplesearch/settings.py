import os
from pathlib import Path

BASE_DIR = Path(__file__).absolute().parents[3]
BASE_DATA_DIR = BASE_DIR / 'data'
assert BASE_DATA_DIR.is_dir()


def datafile(file):
    return BASE_DATA_DIR / file


FILE_NUM = int(os.getenv('FILE_NUM'))
SEARCH_DOCUMENTS_DATA_FILE = datafile(f'news_generated.{FILE_NUM}.csv')
