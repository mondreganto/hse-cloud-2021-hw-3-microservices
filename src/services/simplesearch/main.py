from server import Server
from lib.data_source import CSV
from service import SimpleSearchService
from settings import SEARCH_DOCUMENTS_DATA_FILE


def main():
    search_service = SimpleSearchService(CSV(SEARCH_DOCUMENTS_DATA_FILE))
    server = Server('simplesearch', search_service=search_service)
    server.run_server(debug=True)


if __name__ == '__main__':
    main()
