from typing import Optional

import pandas as pd

from lib.data_source import AbstractDataSource
from lib.search.service import BaseSearchService, CommonSearchMixin


class SimpleSearchService(BaseSearchService, CommonSearchMixin):
    def __init__(self, data_source: AbstractDataSource):
        self._data = pd.DataFrame(
            data_source.read_data(),
            columns=[*self.DOCS_COLUMNS, 'gender', 'age_from', 'age_to', 'region']
        )

    def get_search_data(self, search_text: str, user_data: Optional[dict] = None, limit: int = 10) -> pd.DataFrame:
        # this is some simple algorithm that came to my mind, does not need to be useful or good, just something working
        if search_text is None or search_text == '':
            return pd.DataFrame([], columns=self.DOCS_COLUMNS)
        tokens_count = self._build_tokens_count(search_text)
        gender_mask = self._get_gender_mask(user_data)
        age_mask = self._get_age_mask(user_data)
        rating = gender_mask + age_mask
        df = self._sort_by_rating_and_tokens(rating, tokens_count, self._data['key_md5'])
        return self._data.loc[df.head(limit).index]
