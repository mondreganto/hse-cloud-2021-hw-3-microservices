from flask import Flask, request

from lib.client.search import SearchRequest
from lib.server.healthcheck import HealthCheckMixin
from service import SimpleSearchService


class Server(Flask, HealthCheckMixin):
    def __init__(self, name: str, search_service: SimpleSearchService):
        super().__init__(name)
        self._search_service = search_service
        urls = [
            ('/search', self.search, {'methods': ['POST']}),
            ('/health', self.health, {'methods': ['GET']}),
        ]
        for url in urls:
            if len(url) == 3:
                self.add_url_rule(url[0], url[1].__name__, url[1], **url[2])

    def search(self):
        self.logger.info("Received request json: %s", request.get_json())
        search_request = SearchRequest.parse_obj(request.get_json())
        sr = self._search_service.get_search_data(
            search_text=search_request.search_text,
            user_data=search_request.user_data,
            limit=search_request.limit,
        )
        return sr.to_json()

    def run_server(self, **kwargs):
        super().run(host='0.0.0.0', port=8000, **kwargs)
