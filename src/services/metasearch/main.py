from lib.client.user import UserClient
from lib.client.search import SearchClient
from server import Server
from service import MetaSearchService
from settings import USER_SERVICE_URL, SEARCH_SERVICE_URL


def main():
    user_client = UserClient(url=USER_SERVICE_URL)
    search_client = SearchClient(url=SEARCH_SERVICE_URL)
    metasearch = MetaSearchService(user_client=user_client, search_client=search_client)
    server = Server('metasearch', metasearch=metasearch)
    server.run_server(debug=True)


if __name__ == '__main__':
    main()
