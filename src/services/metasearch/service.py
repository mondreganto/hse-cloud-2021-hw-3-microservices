from typing import List, Dict

from lib.client.search import SearchClient, SearchRequest
from lib.client.user import UserClient


class MetaSearchService:
    def __init__(self, search_client: SearchClient, user_client: UserClient) -> None:
        self._search_client = search_client
        self._user_client = user_client

    def search(self, search_text: str, user_id: int, limit: int = 10) -> List[Dict]:
        user_data = self._user_client.get_data(user_id=user_id)
        search_request = SearchRequest(
            search_text=search_text,
            user_data=user_data,
            limit=limit,
        )
        df = self._search_client.get_data(search_request)
        return df[self._search_client.DOCS_COLUMNS].to_dict('records')
