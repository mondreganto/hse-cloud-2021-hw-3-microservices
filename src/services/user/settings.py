from pathlib import Path

BASE_DIR = Path(__file__).absolute().parents[3]
BASE_DATA_DIR = BASE_DIR / 'data'
assert BASE_DATA_DIR.is_dir()


def datafile(file):
    return BASE_DATA_DIR / file


USER_DATA_FILE = datafile('users.csv')
