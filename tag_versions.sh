#!/bin/bash -e

export METASEARCH_TAG="${CI_COMMIT_SHA}"
export SHARDSEARCH_TAG="${CI_COMMIT_SHA}"
export SIMPLESEARCH1_TAG="${CI_COMMIT_SHA}"
export SIMPLESEARCH2_TAG="${CI_COMMIT_SHA}"
export USER_TAG="${CI_COMMIT_SHA}"

case "${SERVICE}" in
"metasearch")
    export METASEARCH_TAG="${VERSION}"
    ;;

"shardsearch")
    export SHARDSEARCH_TAG="${VERSION}"
    ;;

"simplesearch-shard-1")
    export SIMPLESEARCH1_TAG="${VERSION}"
    ;;

"simplesearch-shard-2")
    export SIMPLESEARCH2_TAG="${VERSION}"
    ;;

"user")
    export USER_TAG="${VERSION}"
    ;;
esac

export METASEARCH_IMAGE="${DOCKER_REGISTRY}/metasearch:${METASEARCH_TAG}"
export SHARDSEARCH_IMAGE="${DOCKER_REGISTRY}/shardsearch:${SHARDSEARCH_TAG}"
export SIMPLESEARCH1_IMAGE="${DOCKER_REGISTRY}/simplesearch-shard-1:${SIMPLESEARCH1_TAG}"
export SIMPLESEARCH2_IMAGE="${DOCKER_REGISTRY}/simplesearch-shard-2:${SIMPLESEARCH2_TAG}"
export USER_IMAGE="${DOCKER_REGISTRY}/user:${USER_TAG}"
