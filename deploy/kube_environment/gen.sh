#!/bin/bash -e

BASE="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"

if [[ -z "${CLUSTER_ID}" ]]; then
    echo "CLUSTER_ID must be set"
    exit 1
fi

CA="${BASE}/ca.pem"
KUBE_CONFIG="${BASE}/kubernetes_config.yaml"
LOCAL_KUBE_CONFIG="${BASE}/localconfig.yml"

echo "[*] Creating service account"
kubectl --kubeconfig="${LOCAL_KUBE_CONFIG}" apply -f "${BASE}/sa.yml"

CLUSTER_DATA="$(yc managed-kubernetes cluster get --id "${CLUSTER_ID}" --format json)"

echo "[*] Fetching certificate authority to ${CA}"
echo "${CLUSTER_DATA}" |
    jq -r .master.master_auth.cluster_ca_certificate |
    awk '{gsub(/\\n/,"\n")}1' >"${CA}"

echo "[*] Generating service account token"
SECRET_NAME="$(kubectl --kubeconfig="${LOCAL_KUBE_CONFIG}" -n kube-system get secret | grep orbit-admin | awk '{print $1}')"
SA_TOKEN="$(kubectl --kubeconfig="${LOCAL_KUBE_CONFIG}" -n kube-system get secret "${SECRET_NAME}" -o json | jq -r .data.token | base64 --d)"

echo "[+] Secret name: ${SECRET_NAME}"
echo "[+] Got token: ${SA_TOKEN}"

echo "[*] Parsing external master endpoint"
MASTER_ENDPOINT="$(echo "${CLUSTER_DATA}" | jq -r .master.endpoints.external_v4_endpoint)"
echo "[+] External master endpoint: ${MASTER_ENDPOINT}"

echo "[*] Setting up cluster orbit"
pushd "${BASE}"
kubectl config set-cluster orbit \
    --certificate-authority="ca.pem" \
    --server="${MASTER_ENDPOINT}" \
    --kubeconfig="${KUBE_CONFIG}"
popd

echo "[*] Setting up credentials for orbit-admin service account"
kubectl config set-credentials orbit-admin \
    --token="${SA_TOKEN}" \
    --kubeconfig="${KUBE_CONFIG}"

echo "[*] Setting up default context"
kubectl config set-context default \
    --cluster=orbit \
    --user=orbit-admin \
    --kubeconfig="${KUBE_CONFIG}"

echo "[*] Setting default config as current"
kubectl config use-context default \
    --kubeconfig="${KUBE_CONFIG}"

echo "[+] Done setting up config for external access"
echo "[+] Final config:"
cat "${KUBE_CONFIG}"

echo "[*] Validating config"
kubectl cluster-info --kubeconfig "${KUBE_CONFIG}"
echo "[+] Validated successfully"

echo "[*] Setting up cluster orbit"
pushd "${BASE}"
kubectl config set-cluster orbit \
    --certificate-authority="ca.pem" \
    --server="${MASTER_ENDPOINT}" \
    --kubeconfig="${KUBE_CONFIG}"
popd
