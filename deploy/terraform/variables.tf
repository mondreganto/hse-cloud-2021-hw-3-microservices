variable "yandex_cloud_token" {
  type        = string
  description = "OAuth token for Yandex Cloud"
}

variable "yandex_cloud_id" {
  type        = string
  description = "Id of the cloud to use"
}

variable "yandex_folder_id" {
  type        = string
  description = "Id of the cloud folder to use"
}

variable "yandex_zone" {
  type        = string
  description = "Zone to use"
}
