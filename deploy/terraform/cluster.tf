resource "yandex_vpc_network" "orbit" {
  name        = "orbit"
  description = "Default network for all resources"
  folder_id   = var.yandex_folder_id
}

resource "yandex_vpc_subnet" "main" {
  name        = "orbit-main"
  description = "subnet for core resources"
  folder_id   = var.yandex_folder_id
  zone        = var.yandex_zone
  network_id  = yandex_vpc_network.orbit.id

  v4_cidr_blocks = ["10.10.0.0/16"]
}

resource "yandex_iam_service_account" "cluster_editor" {
  folder_id = var.yandex_folder_id
  name      = "cluster-manager"
}

resource "yandex_resourcemanager_folder_iam_member" "cluster_editor" {
  folder_id = var.yandex_folder_id
  role      = "editor"
  member    = "serviceAccount:${yandex_iam_service_account.cluster_editor.id}"
}

resource "yandex_kms_symmetric_key" "cluster_main" {
  name        = "cluster-key"
  description = "The key for Kubernetes cluster encryption"
  folder_id   = var.yandex_folder_id
}

resource "yandex_kubernetes_cluster" "hse_hw3" {
  name        = "orbit"
  description = "Main RocketClass cluster"
  folder_id   = var.yandex_folder_id

  network_id = yandex_vpc_network.orbit.id

  master {
    version = "1.20"
    zonal {
      zone      = yandex_vpc_subnet.main.zone
      subnet_id = yandex_vpc_subnet.main.id
    }

    public_ip = true

    security_group_ids = []

    maintenance_policy {
      auto_upgrade = true

      maintenance_window {
        day        = "monday"
        start_time = "3:00"
        duration   = "3h"
      }
    }
  }

  service_account_id      = yandex_iam_service_account.cluster_editor.id
  node_service_account_id = yandex_iam_service_account.cluster_editor.id

  release_channel         = "RAPID"
  network_policy_provider = "CALICO"

  kms_provider {
    key_id = yandex_kms_symmetric_key.cluster_main.id
  }
}

resource "yandex_kubernetes_node_group" "workers" {
  cluster_id  = yandex_kubernetes_cluster.hse_hw3.id
  name        = "runners"
  version     = yandex_kubernetes_cluster.hse_hw3.master[0].version

  node_labels = {
    group_name = "runners"
  }

  instance_template {
    platform_id = "standard-v3"

    //noinspection HCLUnknownBlockType
    network_interface {
      nat                = true
      subnet_ids         = [yandex_vpc_subnet.main.id]
      security_group_ids = []
    }

    resources {
      memory = 4
      cores  = 2
    }

    boot_disk {
      type = "network-hdd"
      size = 64
    }

    scheduling_policy {
      preemptible = true
    }
  }

  scale_policy {
    auto_scale {
      initial = 1
      max     = 5
      min     = 1
    }
  }

  allocation_policy {
    location {
      zone = var.yandex_zone
    }
  }

  maintenance_policy {
    auto_upgrade = true
    auto_repair  = true

    maintenance_window {
      day        = "monday"
      start_time = "3:00"
      duration   = "3h"
    }
  }
}
