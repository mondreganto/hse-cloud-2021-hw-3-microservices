output "registry_id" {
  value = yandex_container_registry.hse_hw3.id
}

output "cluster_id" {
  value = yandex_kubernetes_cluster.hse_hw3.id
}
